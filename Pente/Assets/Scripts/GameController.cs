﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    [SerializeField] float m_minBoundsX;
    [SerializeField] float m_minBoundsY;
    [SerializeField] float m_maxBoundsX;
    [SerializeField] float m_maxBoundsY;

    float m_lineDistanceX;
    float m_lineDistanceY;

    [SerializeField] RectTransform m_Selector;

	[SerializeField] GameObject m_verticalLine;
	[SerializeField] GameObject m_horizontalLine;
	[SerializeField] GameObject m_WhitePiece;
    [SerializeField] GameObject m_BlackPiece;

    [SerializeField] Transform m_PiecesTransformLocation;
    [SerializeField] Transform m_LineTransformLocation;

	[SerializeField] Color m_SelectorColor;
    [SerializeField] Color m_SelectorDeniedColor;

    [SerializeField] GameObject m_TXTWhite;
    [SerializeField] GameObject m_TXTBlack;

    [SerializeField] GameObject m_TriaTXTWhite;
    [SerializeField] GameObject m_TesseraTXTWhite;
    [SerializeField] GameObject m_TriaTXTBlack;
    [SerializeField] GameObject m_TesseraTXTBlack;


    [SerializeField] GameObject m_WinTXTW;
    [SerializeField] GameObject m_WinTXTB;
    [SerializeField] GameObject m_WinTXT;

    [SerializeField] Slider m_Slider;

    public bool m_AiActive = false;

    public bool isPlayerOnesTurn = true;

    public int m_WhiteCaptures = 0;
    public int m_BlackCaptures = 0;

	public int m_boardSize = 19;

    int m_nextBoardSize = 19;

    float m_PiecesScale = 1;

    [SerializeField] Text m_WhiteCaptureTXT;
    [SerializeField] Text m_BlackCaptureTXT;

    [SerializeField] Text m_WhiteName;
    [SerializeField] Text m_BlackName;

    [SerializeField] Text m_BoardSizeTXT;
    [SerializeField] Text m_TimerTXT;

    public bool[,] Cells;

    CanvasRenderer m_SelecterRender;

	bool m_gamePlaying = false;
	bool m_firstTurn = true;

    float m_timer = 20.0f;

    // Use this for initialization
    void Start()
    {
        m_SelecterRender = m_Selector.GetComponent<CanvasRenderer>();
        AdjustBoardSize();
        //init();
    }

	public void PlayGame()
	{
		m_gamePlaying = true;
		init();
	}

	public void PlayAI()
	{
		m_AiActive = true;
		PlayGame();
	}

	public void PlayPlayer()
	{
		m_AiActive = false;
		PlayGame();
	}

	public void BuildBoard(float minX, float minY, float maxX, float maxY, float boardSize, GameObject visualVBorder, GameObject visualHBorder, Transform lineParent)
    {
        foreach (Transform child in lineParent)
        {
            Destroy(child.gameObject);
        }

        m_lineDistanceX = (maxX - minX) / (boardSize-1);
		m_lineDistanceY = (maxY - minY) / (boardSize - 1);

        float pointX = minX;
		float pointY = minY;

        
        pointY -= m_lineDistanceY;

        for (int i=0;i<boardSize;i++)
		{
			if(visualVBorder)
			{
				GameObject obj = Instantiate(visualVBorder,lineParent);
                obj.transform.localPosition = new Vector3(pointX, 0, 0);
				pointY += m_lineDistanceY;
			}

			if (visualHBorder)
            {
                GameObject obj = Instantiate(visualHBorder, lineParent);
                obj.transform.localPosition = new Vector3(0, pointY, 0);
                pointX += m_lineDistanceX;
			}
		}
	}


	public void init()
    {
        m_boardSize = m_nextBoardSize;

        m_WinTXTW.SetActive(false);
        m_WinTXTB.SetActive(false);
        m_WinTXT.SetActive(false);

        Cells = new bool[m_boardSize, m_boardSize];

        foreach (Transform child in m_PiecesTransformLocation)
        {
            Destroy(child.gameObject);
        }

		m_WhiteCaptures = 0;
		m_BlackCaptures = 0;

        BuildBoard(m_minBoundsX, m_minBoundsY, m_maxBoundsX, m_maxBoundsY, m_boardSize, m_verticalLine, m_horizontalLine, m_LineTransformLocation);

        m_PiecesScale = GetPieceScale(m_boardSize);

        m_Selector.localScale = new Vector3(m_PiecesScale, m_PiecesScale, m_PiecesScale);

        m_TriaTXTWhite.SetActive(false);
        m_TriaTXTBlack.SetActive(false);
        m_TesseraTXTWhite.SetActive(false);
        m_TesseraTXTBlack.SetActive(false);

        PlacePiece(new int[] { m_boardSize/2, m_boardSize/2 }, true, m_PiecesTransformLocation);
        isPlayerOnesTurn = false;

        m_TXTWhite.SetActive(!isPlayerOnesTurn);
        m_TXTBlack.SetActive(isPlayerOnesTurn);
    }

    public float GetPieceScale(int boardSize)
    {
        float toReturn = 0;

        if (boardSize < 19)
        {
            toReturn = Mathf.Lerp(2.0f, 1.0f, (boardSize - 9) / 11);
        }
        else
        {
            toReturn = Mathf.Lerp(1.0f, 0.5f, (boardSize - 19) / 20);
        }

        return toReturn;
    }

    // Update is called once per frame
    void Update()
    {
        m_WhiteCaptureTXT.text = m_WhiteCaptures.ToString();
        m_BlackCaptureTXT.text = m_BlackCaptures.ToString();

        float mouseRatioX = Input.mousePosition.x / Screen.width;
        mouseRatioX = (2.0f * mouseRatioX) - 1.0f;
        float mouseRatioY = Input.mousePosition.y / Screen.height;
        mouseRatioY = (2.0f * mouseRatioY) - 1.0f;

        float MouseXToCanvas = mouseRatioX * (2560 / 2);
        float MouseYToCanvas = mouseRatioY * (1440 / 2);

		if (m_gamePlaying)
        {
            int[] pos = GridPosition(MouseXToCanvas, MouseYToCanvas);

            float newX = (pos[0] * m_lineDistanceX) + m_minBoundsX;
            float newY = (pos[1] * m_lineDistanceY) + m_minBoundsY;

            // Debug.Log(newX);

            m_Selector.transform.localPosition = new Vector3(newX, newY, 0);

            if (m_AiActive && !isPlayerOnesTurn)
			{
				int[] choice = GetAITurn(Cells);

				Piece placedPiece = PlacePiece(choice, isPlayerOnesTurn, m_PiecesTransformLocation);
				Capture(placedPiece, m_PiecesTransformLocation);
				if (gameWin(m_PiecesTransformLocation))
				{
					m_gamePlaying = false;
					m_AiActive = false;
				}
				isPlayerOnesTurn = !isPlayerOnesTurn;

				m_TXTWhite.SetActive(!isPlayerOnesTurn);
				m_TXTBlack.SetActive(isPlayerOnesTurn);
			}
			else
			{
				bool canPlaceHere = canPlace(pos, Cells);

				if (m_firstTurn && isPlayerOnesTurn) canPlaceHere = canPlaceFirst(pos, Cells);

				if (canPlaceHere)
				{
					m_SelecterRender.SetColor(m_SelectorColor);
					if (Input.GetMouseButtonDown(0))
					{
						if (m_firstTurn && isPlayerOnesTurn) m_firstTurn = false;

						m_TriaTXTWhite.SetActive(false);
						m_TriaTXTBlack.SetActive(false);
						m_TesseraTXTWhite.SetActive(false);
						m_TesseraTXTBlack.SetActive(false);

						Piece placedPiece = PlacePiece(pos, isPlayerOnesTurn, m_PiecesTransformLocation);
						Capture(placedPiece, m_PiecesTransformLocation);
						if(gameWin(m_PiecesTransformLocation))
						{
							m_gamePlaying = false;
						}
						isPlayerOnesTurn = !isPlayerOnesTurn;

						m_TXTWhite.SetActive(!isPlayerOnesTurn);
						m_TXTBlack.SetActive(isPlayerOnesTurn);

					}
				}
				else
				{
					m_SelecterRender.SetColor(m_SelectorDeniedColor);
				}
			}

            m_timer -= Time.deltaTime;

            m_TimerTXT.text = "Timer: " + m_timer.ToString("0.0");

            if (m_timer <= 0)
            {
                TimerExpired();
            }
		}
	}

    public void TimerExpired()
    {
        m_timer = 20.0f;

        isPlayerOnesTurn = !isPlayerOnesTurn;

        m_TXTWhite.SetActive(!isPlayerOnesTurn);
        m_TXTBlack.SetActive(isPlayerOnesTurn);
    }

	private bool canPlaceFirst(int[] pos, bool[,] cells)
	{
		if (pos[0] < m_boardSize / 2 - 3 || pos[0] > m_boardSize / 2 + 3 || pos[1] < m_boardSize / 2 - 3 || pos[1] > m_boardSize / 2 + 3) return false;
		return canPlace(pos, cells);
	}

	public int[] GetAITurn(bool[,] board)
	{
		int[] choice = new int[2];
		bool valid = false;

		while(!valid)
		{
			int x = UnityEngine.Random.Range(0, board.GetLength(0) - 1);
			int y = UnityEngine.Random.Range(0, board.GetLength(0) - 1);

			if (!board[x, y])
			{
				choice[0] = x;
				choice[1] = y;
				valid = true;
			}
		}

		return choice;
	}

    public bool gameWin(Transform piecesParent)
    {
        if (m_WhiteCaptures >= 5)
        {//white wins
            Debug.Log("Game win yehya");

            if (m_WinTXTW)
            {
                m_WinTXTW.SetActive(true);

                if (m_WhiteName.text == "" || m_WhiteName.text.Length == 0)
                {
                    m_WinTXTW.GetComponent<Text>().text = "Player One";
                }
                else
                    m_WinTXTW.GetComponent<Text>().text = m_WhiteName.text;
            }

			if(m_WinTXT)
            m_WinTXT.SetActive(true);
            return true;
        }
        else if(m_BlackCaptures >= 5)
        {//Black wins
            Debug.Log("Game win yehya");

            if (m_WinTXTB)
            {
                m_WinTXTB.SetActive(true);

                if (m_BlackName.text == "" || m_BlackName.text.Length == 0)
                {
                    m_WinTXTB.GetComponent<Text>().text = "Player One";
                }
                else
                    m_WinTXTB.GetComponent<Text>().text = m_BlackName.text;
            }

			if(m_WinTXT)
            m_WinTXT.SetActive(true);
            return true;
        }

        Piece[] pieces = piecesParent.GetComponentsInChildren<Piece>();

        foreach (Piece p in pieces)
        {

            foreach (Piece p2 in pieces)
            {
                int DirX = (p.m_XPos - p2.m_XPos);
                int DirY = (p.m_YPos - p2.m_YPos);

                if (p2.isWhite == p.isWhite)
                {
                    if(!(DirX == 0 && DirY == 0))
                        if ((DirX == 1 || DirX == 0 || DirX == -1) && (DirY == 1 || DirY == 0 || DirY == -1))
                        {
                            triaSpace(p, p2, pieces);
                            foreach (Piece p3 in pieces)
                            {
                                if ((p3.isWhite == p2.isWhite) && (p2.m_XPos - DirX == p3.m_XPos && p2.m_YPos - DirY == p3.m_YPos))
                                {
                                    tria(p, p2, p3, pieces);

                                    foreach (Piece p4 in pieces)
                                    {
                                        if ((p4.isWhite == p3.isWhite) && (p3.m_XPos - DirX == p4.m_XPos && p3.m_YPos - DirY == p4.m_YPos))
                                        {
                                            tessera(p, p2, p3, p4, pieces);

                                            foreach (Piece p5 in pieces)
                                            {
                                                if ((p4.isWhite == p5.isWhite) && (p4.m_XPos - DirX == p5.m_XPos && p4.m_YPos - DirY == p5.m_YPos))
                                                {
                                                    Debug.Log("Game win yehya");

                                                    if (p.isWhite)
                                                    {                                                        
                                                        if (m_WhiteName.text == "" || m_WhiteName.text.Length == 0)
                                                        {
                                                            m_WinTXTW.GetComponent<Text>().text = "Player One";
                                                        }
                                                        else
                                                            m_WinTXTW.GetComponent<Text>().text = m_WhiteName.text;

                                                        m_WinTXTW.SetActive(true);
                                                        m_WinTXT.SetActive(true);
                                                    }
                                                    else
                                                    {
                                                        Debug.Log(m_BlackName.text.Length);
                                                        if (m_BlackName.text == "" || m_BlackName.text.Length == 0)
                                                        {
                                                            m_WinTXTB.GetComponent<Text>().text = "Player One";
                                                        }
                                                        else
                                                            m_WinTXTB.GetComponent<Text>().text = m_BlackName.text;

                                                        m_WinTXTB.SetActive(true);
                                                        m_WinTXT.SetActive(true);
                                                    }

                                                    return true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                }
            }
        }

        return false;
    }
    public bool triaSpace(Piece one, Piece two, Piece[] allPieces)
    {
        int DirX = (one.m_XPos - two.m_XPos);
        int DirY = (one.m_YPos - two.m_YPos);


        foreach (Piece p in allPieces)
        {
            if ((((p.m_XPos - (2 * DirX)) == one.m_XPos) && ((p.m_YPos - (2 * DirY)) == one.m_YPos)) ||
                ((p.m_XPos + (2 * DirX)) == two.m_XPos) && ((p.m_YPos + (2 * DirY)) == two.m_YPos))
            {
                if (p.isWhite == one.isWhite)
                {
                    int BlockedSides = 0;

                    int BetweenX = 0;
                    int BetweenY = 0;

                    if (BlockedSides < 1)
                    {
                        if ((p.m_XPos - (2 * DirX)) == one.m_XPos)
                        {
                            BetweenX = (p.m_XPos + one.m_XPos) / 2;
                            BetweenY = (p.m_YPos + one.m_YPos) / 2;
                        }
                        else
                        {
                            BetweenX = (p.m_XPos + two.m_XPos) / 2;
                            BetweenY = (p.m_YPos + two.m_YPos) / 2;
                        }
                    }

                    foreach (Piece other in allPieces)
                    {
                        if (other.m_XPos == BetweenX && other.m_YPos == BetweenY)
                        {
                            BlockedSides += 10;
                            break;
                        }

                        if ((((other.m_XPos - DirX) == one.m_XPos) && ((other.m_YPos - DirY) == one.m_YPos)) ||
                            ((other.m_XPos + DirX) == two.m_XPos) && ((other.m_YPos + DirY) == two.m_YPos))
                        {
                            BlockedSides++;
                        }
                    }

                    if (BlockedSides < 1)
                    {
                        if (one.isWhite)
                        {
                            m_TriaTXTWhite.SetActive(true);
                        }
                        else
                        {
                            m_TriaTXTBlack.SetActive(true);
                        }

						return true;
                    }
                }
            }
        }

		return false;
    }

    public bool tria(Piece one, Piece two, Piece three, Piece[] allPieces)
    {
        int DirX = (one.m_XPos - two.m_XPos);
        int DirY = (one.m_YPos - two.m_YPos);

        int BlockedSides = 0;

        foreach (Piece other in allPieces)
        {
            if ((((other.m_XPos - DirX) == one.m_XPos) && ((other.m_YPos - DirY) == one.m_YPos)) ||
                ((other.m_XPos + DirX) == three.m_XPos) && ((other.m_YPos + DirY) == three.m_YPos))
            {
                BlockedSides++;
            }
        }

		if (BlockedSides < 1)
		{
			if (one.isWhite)
			{
				m_TriaTXTWhite.SetActive(true);
			}
			else
			{
				m_TriaTXTBlack.SetActive(true);
			}

			return true;
		}

		return false;
    }

    public void RulesBTN()
    {
        Application.OpenURL("https://www.pente.net/instructions.html");
    }

    public void AdjustBoardSize()
    {
        if (m_Slider.value % 2 == 0)
            m_Slider.value++;

        m_nextBoardSize = (int)m_Slider.value;

        m_BoardSizeTXT.text = "BoardSize: " + m_nextBoardSize;


    }

    public bool tessera(Piece one, Piece two, Piece three, Piece four, Piece[] allPieces)
    {
        int DirX = (one.m_XPos - two.m_XPos);
        int DirY = (one.m_YPos - two.m_YPos);

        int BlockedSides = 0;

        foreach (Piece other in allPieces)
        {
            if ((((other.m_XPos - DirX) == one.m_XPos) && ((other.m_YPos - DirY) == one.m_YPos)) ||
                ((other.m_XPos + DirX) == four.m_XPos) && ((other.m_YPos + DirY) == four.m_YPos))
            {
                BlockedSides++;
            }
        }

        if (BlockedSides < 2)
        {
            if (one.isWhite)
            {
                m_TesseraTXTWhite.SetActive(true);
            }
            else
            {
                m_TesseraTXTBlack.SetActive(true);
            }

			return true;
        }

		return false;
    }

    /// <summary>
    /// Capture pieces logic
    /// </summary>
    /// <param name="PlacedPiece">The piece that was placed</param>
    /// <param name="piecesParent">The trransform in which all placed pieces objects are tied to</param>
    /// <returns>true if pieces were captured</returns>
    public bool Capture(Piece PlacedPiece, Transform piecesParent)
    {
		if (piecesParent == null) return false;

        bool toReturn = false;        

        Piece[] pieces = piecesParent.GetComponentsInChildren<Piece>();

        List<Piece> CaptureAttempts = GetCaptureAttempts(PlacedPiece, pieces);


        foreach (Piece p in CaptureAttempts)
        {
            Piece[] toCapture = GetCapturedPieces(PlacedPiece, p, pieces);

            if (toCapture[0] != null && toCapture[1] != null)
            {
                toReturn = true;

                Cells[toCapture[0].m_XPos, toCapture[0].m_YPos] = false;
                Cells[toCapture[1].m_XPos, toCapture[1].m_YPos] = false;

                Destroy(toCapture[0].gameObject);
                Destroy(toCapture[1].gameObject);
                //increment player capture count for correct player.

                if (isPlayerOnesTurn)
                {
                    m_BlackCaptures++;
                }
                else
                {
                    m_WhiteCaptures++;
                }
            }
        }

        return toReturn;
    }

    /// <summary>
    /// From the placedpiece to the capturedestination, returns an array of two pieces if the pieces
    /// are in the correct direction and of the correct color
    /// </summary>
    /// <param name="PlacedPiece">The piece that was placed</param>
    /// <param name="captureDestination">the piece that represents the capture direction from the placed piece</param>
    /// <param name="pieces">an array of all pieces on the board</param>
    /// <returns>an array of two pieces that are to be captured</returns>
    public Piece[] GetCapturedPieces(Piece PlacedPiece, Piece captureDestination, Piece[] pieces)
    {
        Piece[] toCapture = new Piece[] { null, null };

        int DirX = (captureDestination.m_XPos - PlacedPiece.m_XPos) / 3;
        int DirY = (captureDestination.m_YPos - PlacedPiece.m_YPos) / 3;
        int count = 0;
        foreach (Piece toCap in pieces)
        {
            if (toCap.isWhite != PlacedPiece.isWhite &&
                (toCap.m_XPos == PlacedPiece.m_XPos + DirX && toCap.m_YPos == PlacedPiece.m_YPos + DirY ||
                toCap.m_XPos == PlacedPiece.m_XPos + (2 * DirX) && toCap.m_YPos == PlacedPiece.m_YPos + (2 * DirY)))
            {
                toCapture[count] = toCap;
                count++;
            }
        }

        return toCapture;
    }
   
	/// <summary>
    /// From the placed piece, looks for other pieces of the same color in the 8 cardinal directions
    /// as destinations for captureAttempts
    /// </summary>
    /// <param name="PlacedPiece">The placed piece</param>
    /// <param name="pieces">All other pieces on the board</param>
    /// <returns>a list of pieces as capture attempt directions from the placed piece</returns>
    public List<Piece> GetCaptureAttempts(Piece PlacedPiece, Piece[] pieces)
    {
        List<Piece> CaptureAttempts = new List<Piece>();

        foreach (Piece p in pieces)
        {
            if (p.isWhite == PlacedPiece.isWhite)
            {
                bool XAt3 = false;
                if (p.m_XPos == (PlacedPiece.m_XPos + 3) || (p.m_XPos == (PlacedPiece.m_XPos - 3)))
                {
                    XAt3 = true;
                }

                bool YAt3 = false;

                if (p.m_YPos == (PlacedPiece.m_YPos + 3) || (p.m_YPos == (PlacedPiece.m_YPos - 3)))
                {
                    YAt3 = true;
                }

                if (XAt3 && YAt3)
                {
                    CaptureAttempts.Add(p);
                }
                else if (XAt3 && p.m_YPos == PlacedPiece.m_YPos)
                {
                    CaptureAttempts.Add(p);
                }
                else if (YAt3 && p.m_XPos == PlacedPiece.m_XPos)
                {
                    CaptureAttempts.Add(p);
                }
            }
        }

        return CaptureAttempts;
    }

    /// <summary>
    /// If the position has been taken or not
    /// </summary>
    /// <param name="cellPlacement">Where you want to place</param>
    /// <param name="cellsMaster">The grid</param>
    /// <returns></returns>
    public bool canPlace(int[] cellPlacement, bool[,] cellsMaster)
    {
        return !cellsMaster[cellPlacement[0], cellPlacement[1]];
    }

    Piece PlacePiece(int[] cellPlacement, bool isWhite, Transform piecesParent)
    {
        Cells[cellPlacement[0], cellPlacement[1]] = true;
        GameObject obj = null;

        if (!isWhite)
        {
			if(m_WhitePiece)
            obj = GameObject.Instantiate(m_WhitePiece, piecesParent);
        }
        else
        {
			if(m_BlackPiece)
            obj = GameObject.Instantiate(m_BlackPiece, piecesParent);
        }

        float newX = (cellPlacement[0] * m_lineDistanceX) + m_minBoundsX;
        float newY = (cellPlacement[1] * m_lineDistanceY) + m_minBoundsY;

		if(obj) 
        obj.transform.localPosition = new Vector3(newX, newY, 0);
        obj.transform.localScale = new Vector3(m_PiecesScale, m_PiecesScale, m_PiecesScale);

        obj.GetComponent<Piece>().givePos(cellPlacement);

        m_timer = 20.0f;

        return obj.GetComponent<Piece>();
    }

    /// <summary>
    /// Returns an array of 2 ints between 0 and 18 each dependent on the input
    /// the min bounds and the line distances
    /// </summary>
    /// <param name="x">MouseX position to CanvasX position inpuut</param>
    /// <param name="y">MouseX position to CanvasX position inpuut</param>
    /// <returns></returns>
    int[] GridPosition(float x, float y)
    {
        //Debug.Log("Mouse Position: " + x + " , " + y);
        int[] toReturn = new int[] { 0, 0 };
        if (x > m_minBoundsX)
        {
            float testX = x - m_minBoundsX;

            while (testX > 25 * m_PiecesScale)
            {
                testX -= m_lineDistanceX;
                toReturn[0]++;
            }
            if (toReturn[0] > (m_boardSize - 1)) toReturn[0] = m_boardSize - 1;
        }
        if (y > m_minBoundsY)
        {
            float testY = y - m_minBoundsY;

            while (testY > 50 * m_PiecesScale)
            {
                testY -= m_lineDistanceX;
                toReturn[1]++;
            }

            if (toReturn[1] > (m_boardSize - 1)) toReturn[1] = m_boardSize - 1;
        }

        return toReturn;
    }
}
