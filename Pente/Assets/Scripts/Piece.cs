﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piece : MonoBehaviour {

    public bool isWhite;

    public int m_XPos;
    public int m_YPos;

    public void givePos(int[] pos)
    {
        m_XPos = pos[0];
        m_YPos = pos[1];
    }
}
