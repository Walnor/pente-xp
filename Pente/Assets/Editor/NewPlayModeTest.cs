﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

public class NewPlayModeTest {

    [UnityTest]

    //can capture left
    //can capture right
    //can capture up
    //can capture down
    //can capture diagonal 1
    //can capture diagonal 2
    //can capture diagonal 3
    //can capture diagonal 4
    public IEnumerator CanCaptureTest()
    {
        GameObject controller = new GameObject();
        GameController test = controller.AddComponent<GameController>() as GameController;
        test.Cells = new bool[19, 19];
        GameObject gameObject = new GameObject();
        Piece capturedPiece1 = gameObject.AddComponent<Piece>() as Piece;
        capturedPiece1.isWhite = false;
        capturedPiece1.m_XPos = 10;
        capturedPiece1.m_YPos = 9;

        GameObject gameObject2 = new GameObject();
        Piece capturedPiece2 = gameObject2.AddComponent<Piece>() as Piece;
        capturedPiece2.isWhite = false;
        capturedPiece2.m_XPos = 11;
        capturedPiece2.m_YPos = 9;

        GameObject gameObject3 = new GameObject();
        Piece capturePiece1 = gameObject3.AddComponent<Piece>() as Piece;
        capturePiece1.isWhite = true;
        capturePiece1.m_XPos = 12;
        capturePiece1.m_YPos = 9;

        GameObject gameObject4 = new GameObject();
        Piece capturePiece2 = gameObject4.AddComponent<Piece>() as Piece;
        capturePiece2.isWhite = true;
        capturePiece2.m_XPos = 9;
        capturePiece2.m_YPos = 9;

        GameObject transformParent = new GameObject();
        gameObject.transform.parent = transformParent.transform;
        gameObject2.transform.parent = transformParent.transform;
        gameObject3.transform.parent = transformParent.transform;
        gameObject4.transform.parent = transformParent.transform;
        foreach (Piece ob in transformParent.GetComponentsInChildren<Piece>())
        {
            Debug.Log(ob.isWhite + " " + ob.m_XPos + " " + ob.m_YPos);
        }
        Assert.True(test.Capture(capturePiece2, transformParent.transform));
        yield return null;
    }

    [UnityTest]

    //timer start
    //timer end
    //timer pause
    public IEnumerator TimerExpireTest()
    {
        GameObject controller = new GameObject();
        GameController test = controller.AddComponent<GameController>() as GameController;
        test.isPlayerOnesTurn = true;
        //bool result = test.TimerExpire();
        //Assert.False(result);
        yield return null;
    }

    //menu popup test
    //menu click test
    //menu timer pause test
    [UnityTest]
    public IEnumerator MenuPauseTest()
    {
        GameObject controller = new GameObject();
        GameController test = controller.AddComponent<GameController>() as GameController;

        yield return null;
    }

    [UnityTest]

    public IEnumerator SaveBoardTest()
    {
        GameObject controller = new GameObject();
        GameController test = controller.AddComponent<GameController>() as GameController;

        yield return null;
    }

    //load test

    [UnityTest]
    public IEnumerator WinByPlacingGameTest()
    {
        GameObject controller = new GameObject();
        GameController test = controller.AddComponent<GameController>() as GameController;

        test.Cells = new bool[19, 19];

        GameObject gameObject = new GameObject();
        Piece Piece1 = gameObject.AddComponent<Piece>() as Piece;
        Piece1.isWhite = true;
        Piece1.m_XPos = 10;
        Piece1.m_YPos = 9;

        GameObject gameObject2 = new GameObject();
        Piece Piece2 = gameObject2.AddComponent<Piece>() as Piece;
        Piece2.isWhite = true;
        Piece2.m_XPos = 11;
        Piece2.m_YPos = 9;

        GameObject gameObject3 = new GameObject();
        Piece Piece3 = gameObject3.AddComponent<Piece>() as Piece;
        Piece3.isWhite = true;
        Piece3.m_XPos = 12;
        Piece3.m_YPos = 9;

        GameObject gameObject4 = new GameObject();
        Piece Piece4 = gameObject4.AddComponent<Piece>() as Piece;
        Piece4.isWhite = true;
        Piece4.m_XPos = 13;
        Piece4.m_YPos = 9;

        GameObject gameObject5 = new GameObject();
        Piece Piece5 = gameObject5.AddComponent<Piece>() as Piece;
        Piece5.isWhite = true;
        Piece5.m_XPos = 14;
        Piece5.m_YPos = 9;

        GameObject transformParent = new GameObject();
        gameObject.transform.parent = transformParent.transform;
        gameObject2.transform.parent = transformParent.transform;
        gameObject3.transform.parent = transformParent.transform;
        gameObject4.transform.parent = transformParent.transform;
        gameObject5.transform.parent = transformParent.transform;

        Assert.True(test.gameWin(transformParent.transform));

        yield return null;
    }

    [UnityTest]

    //win equal to 5
    //win greater than 5
    //win less than 5
    public IEnumerator WinByCaptureTest()
    {
        GameObject controller = new GameObject();
        GameController test = controller.AddComponent<GameController>() as GameController;

        test.m_WhiteCaptures = 5;
        GameObject transformParent = new GameObject();

        Assert.True(test.gameWin(transformParent.transform));

        yield return null;
    }

    [UnityTest]
    public IEnumerator TriaTest()
    {
        GameObject controller = new GameObject();
        GameController test = controller.AddComponent<GameController>() as GameController;

        test.Cells = new bool[19, 19];

        GameObject gameObject = new GameObject();
        Piece Piece1 = gameObject.AddComponent<Piece>() as Piece;
        Piece1.isWhite = true;
        Piece1.m_XPos = 10;
        Piece1.m_YPos = 9;

        GameObject gameObject2 = new GameObject();
        Piece Piece2 = gameObject2.AddComponent<Piece>() as Piece;
        Piece2.isWhite = true;
        Piece2.m_XPos = 11;
        Piece2.m_YPos = 9;

        GameObject gameObject3 = new GameObject();
        Piece Piece3 = gameObject3.AddComponent<Piece>() as Piece;
        Piece3.isWhite = true;
        Piece3.m_XPos = 12;
        Piece3.m_YPos = 9;

        GameObject transformParent = new GameObject();
        gameObject.transform.parent = transformParent.transform;
        gameObject2.transform.parent = transformParent.transform;
        gameObject3.transform.parent = transformParent.transform;
       
        Piece[] pieces = transformParent.transform.GetComponentsInChildren<Piece>();

        Assert.True(test.tria(Piece1,Piece2, Piece3, pieces));

        yield return null;
    }

    [UnityTest]

    //tria left
    //tria right
    //tria up
    //tria down
    //tria diagnol 1
    //tria diagnol 2
    //tria diagnol 3
    //tria diagnol 4
    //less than 3
    //greater than 3
    //equal to 3
    //gap test
    //tria announce test
    public IEnumerator TriaSpaceTest()
    {
        GameObject controller = new GameObject();
        GameController test = controller.AddComponent<GameController>() as GameController;

        test.Cells = new bool[19, 19];


        GameObject gameObject = new GameObject();
        Piece Piece1 = gameObject.AddComponent<Piece>() as Piece;
        Piece1.isWhite = true;
        Piece1.m_XPos = 10;
        Piece1.m_YPos = 9;

        GameObject gameObject2 = new GameObject();
        Piece Piece2 = gameObject2.AddComponent<Piece>() as Piece;
        Piece2.isWhite = true;
        Piece2.m_XPos = 11;
        Piece2.m_YPos = 9;

        GameObject gameObject3 = new GameObject();
        Piece Piece3 = gameObject3.AddComponent<Piece>() as Piece;
        Piece3.isWhite = true;
        Piece3.m_XPos = 13;
        Piece3.m_YPos = 9;

        GameObject transformParent = new GameObject();
        gameObject.transform.parent = transformParent.transform;
        gameObject2.transform.parent = transformParent.transform;
        gameObject3.transform.parent = transformParent.transform;

        Piece[] pieces = transformParent.transform.GetComponentsInChildren<Piece>();

        Assert.True(test.triaSpace(Piece1, Piece2, pieces));

        yield return null;
    }

    [UnityTest]

    //tesera up
    public IEnumerator TesseraVerticalTest()
    {
        GameObject controller = new GameObject();
        GameController test = controller.AddComponent<GameController>() as GameController;

        test.Cells = new bool[19, 19];

        GameObject gameObject = new GameObject();
        Piece Piece1 = gameObject.AddComponent<Piece>() as Piece;
        Piece1.isWhite = true;
        Piece1.m_XPos = 10;
        Piece1.m_YPos = 9;

        GameObject gameObject2 = new GameObject();
        Piece Piece2 = gameObject2.AddComponent<Piece>() as Piece;
        Piece2.isWhite = true;
        Piece2.m_XPos = 11;
        Piece2.m_YPos = 9;

        GameObject gameObject3 = new GameObject();
        Piece Piece3 = gameObject3.AddComponent<Piece>() as Piece;
        Piece3.isWhite = true;
        Piece3.m_XPos = 12;
        Piece3.m_YPos = 9;

        GameObject gameObject4 = new GameObject();
        Piece Piece4 = gameObject4.AddComponent<Piece>() as Piece;
        Piece4.isWhite = true;
        Piece4.m_XPos = 13;
        Piece4.m_YPos = 9;

        GameObject gameObject5 = new GameObject();
        Piece Piece5 = gameObject5.AddComponent<Piece>() as Piece;
        Piece5.isWhite = false;
        Piece5.m_XPos = 14;
        Piece5.m_YPos = 9;

        GameObject transformParent = new GameObject();
        gameObject.transform.parent = transformParent.transform;
        gameObject2.transform.parent = transformParent.transform;
        gameObject3.transform.parent = transformParent.transform;
        gameObject4.transform.parent = transformParent.transform;
        gameObject5.transform.parent = transformParent.transform;

        Piece[] pieces = transformParent.transform.GetComponentsInChildren<Piece>();
        Assert.True(test.tessera(Piece1, Piece2, Piece3, Piece4, pieces));

        yield return null;
    }
    //tessera down

    //tessera left
    public IEnumerator TesseraHorizontalLeftPlaceTest()
    {
        GameObject controller = new GameObject();
        GameController test = controller.AddComponent<GameController>() as GameController;

        test.Cells = new bool[19, 19];

        GameObject gameObject = new GameObject();
        Piece Piece1 = gameObject.AddComponent<Piece>() as Piece;
        Piece1.isWhite = true;
        Piece1.m_XPos = 10;
        Piece1.m_YPos = 9;

        GameObject gameObject2 = new GameObject();
        Piece Piece2 = gameObject2.AddComponent<Piece>() as Piece;
        Piece2.isWhite = true;
        Piece2.m_XPos = 11;
        Piece2.m_YPos = 9;

        GameObject gameObject3 = new GameObject();
        Piece Piece3 = gameObject3.AddComponent<Piece>() as Piece;
        Piece3.isWhite = true;
        Piece3.m_XPos = 12;
        Piece3.m_YPos = 9;

        GameObject gameObject4 = new GameObject();
        Piece Piece4 = gameObject4.AddComponent<Piece>() as Piece;
        Piece4.isWhite = true;
        Piece4.m_XPos = 13;
        Piece4.m_YPos = 9;

        GameObject transformParent = new GameObject();
        gameObject.transform.parent = transformParent.transform;
        gameObject2.transform.parent = transformParent.transform;
        gameObject3.transform.parent = transformParent.transform;
        gameObject4.transform.parent = transformParent.transform;


        Piece[] pieces = transformParent.transform.GetComponentsInChildren<Piece>();
        Assert.True(test.tessera(Piece1, Piece2, Piece3, Piece4, pieces));

        yield return null;
    }
    //tessera right
    public IEnumerator TesseraHorizontalRightPlaceTest()
    {
        GameObject controller = new GameObject();
        GameController test = controller.AddComponent<GameController>() as GameController;

        test.Cells = new bool[19, 19];

        GameObject gameObject = new GameObject();
        Piece Piece1 = gameObject.AddComponent<Piece>() as Piece;
        Piece1.isWhite = true;
        Piece1.m_XPos = 10;
        Piece1.m_YPos = 9;

        GameObject gameObject2 = new GameObject();
        Piece Piece2 = gameObject2.AddComponent<Piece>() as Piece;
        Piece2.isWhite = true;
        Piece2.m_XPos = 11;
        Piece2.m_YPos = 9;

        GameObject gameObject3 = new GameObject();
        Piece Piece3 = gameObject3.AddComponent<Piece>() as Piece;
        Piece3.isWhite = true;
        Piece3.m_XPos = 12;
        Piece3.m_YPos = 9;

        GameObject gameObject4 = new GameObject();
        Piece Piece4 = gameObject4.AddComponent<Piece>() as Piece;
        Piece4.isWhite = true;
        Piece4.m_XPos = 13;
        Piece4.m_YPos = 9;

        GameObject transformParent = new GameObject();
        gameObject.transform.parent = transformParent.transform;
        gameObject2.transform.parent = transformParent.transform;
        gameObject3.transform.parent = transformParent.transform;
        gameObject4.transform.parent = transformParent.transform;


        Piece[] pieces = transformParent.transform.GetComponentsInChildren<Piece>();
        Assert.True(test.tessera(Piece1, Piece2, Piece3, Piece4, pieces));

        yield return null;
    }
    //tessera diagonal 1
    //tessera diagonal 2
    //tessera diagonal 3
    //tessera diagonal 4
    //greater than 4
    //equal to 4
    //less than 4h
    public IEnumerator TesseraTest()
    {
        GameObject controller = new GameObject();
        GameController test = controller.AddComponent<GameController>() as GameController;

        test.Cells = new bool[19, 19];

        GameObject gameObject = new GameObject();
        Piece Piece1 = gameObject.AddComponent<Piece>() as Piece;
        Piece1.isWhite = true;
        Piece1.m_XPos = 10;
        Piece1.m_YPos = 9;

        GameObject gameObject2 = new GameObject();
        Piece Piece2 = gameObject2.AddComponent<Piece>() as Piece;
        Piece2.isWhite = true;
        Piece2.m_XPos = 11;
        Piece2.m_YPos = 9;

        GameObject gameObject3 = new GameObject();
        Piece Piece3 = gameObject3.AddComponent<Piece>() as Piece;
        Piece3.isWhite = true;
        Piece3.m_XPos = 12;
        Piece3.m_YPos = 9;

        GameObject gameObject4 = new GameObject();
        Piece Piece4 = gameObject4.AddComponent<Piece>() as Piece;
        Piece4.isWhite = true;
        Piece4.m_XPos = 13;
        Piece4.m_YPos = 9;

        GameObject gameObject5 = new GameObject();
        Piece Piece5 = gameObject5.AddComponent<Piece>() as Piece;
        Piece5.isWhite = false;
        Piece5.m_XPos = 14;
        Piece5.m_YPos = 9;

        GameObject transformParent = new GameObject();
        gameObject.transform.parent = transformParent.transform;
        gameObject2.transform.parent = transformParent.transform;
        gameObject3.transform.parent = transformParent.transform;
        gameObject4.transform.parent = transformParent.transform;
        gameObject5.transform.parent = transformParent.transform;

        Piece[] pieces = transformParent.transform.GetComponentsInChildren<Piece>();
        Assert.True(test.tessera(Piece1, Piece2, Piece3, Piece4, pieces));

        yield return null;
    }

    //board test 
    //odd test
    //even test
    //adjust test


    //turn test
    //black first
    //change player test
}
