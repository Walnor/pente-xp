﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System.Timers;
using System;

public class NewEditModeTest
{

    [UnityTest]
    public IEnumerator CanPlacePieceTest()
    {
        GameObject controller = new GameObject();
        GameController test = controller.AddComponent<GameController>() as GameController;
        bool[,] grid = new bool[19, 19];
        int[] pos = new int[] { 10, 10 };
        if (!test.canPlace(pos, grid)) { Assert.Fail(); }
        yield return null;
    }
}